package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/reiver/go-telnet"
	"github.com/sparrc/go-ping"
)

// функция получает IP адрес устройства и время в секундах, в течении которого
// производиться попытки получить к нему доступ
// как только устройство ответит на пинг функция возвращает true
// если ответ на пинг не был получен или произошла ошика при запуске пинга,
// функция возвращает false
func Pinger(ip_add string, duration int) (result bool) {
	pinger, err := ping.NewPinger(ip_add)
	pinger.SetPrivileged(true)
	pinger.Interval = 500 * time.Millisecond
	pinger.Timeout = time.Second * time.Duration(duration)

	if err != nil {
		fmt.Printf("ERROR: %s\n", err.Error())
		result = false
	}

	pinger.OnRecv = func(pkt *ping.Packet) {
		fmt.Printf("Response received!\n")
		pinger.Stop()
		result = true
	}

	pinger.OnFinish = func(stats *ping.Statistics) {
		if stats.PacketsRecv == 0 {
			fmt.Printf("No response in %v seconds\n", duration)
			result = false
		}
	}

	fmt.Printf("Waiting for %v to respond...\n", ip_add)
	pinger.Run()
	return result
}

// функция получает в качестве аргумента строку с информацией, которую следует
// запросить у пользователя и возвращает введённую пользователем информацию
func Requester(request string) string {
	fmt.Printf("Enter %v: ", request)
	fmt.Print("\n")
	return Reader()
}

// функция считывает пользовательский ввод из stdin
func Reader() (result string) {

	scanner := bufio.NewScanner(os.Stdin)
	if scanner.Scan() {
		result = scanner.Text()
	}
	if err := scanner.Err(); err != nil {
		result = ""
	}

	return result
}

// функция считывает данные из сессии conn пока не возникнет ошибка,
// или метод Read() не вернёт пустой буфер или пока не будет считана
// последовательности символов, полученная в переменной expect
func ReaderTelnet(conn *telnet.Conn, expect string) (out string) {
	var buffer [1]byte
	recvData := buffer[:]
	var n int
	var err error

	for {
		n, err = conn.Read(recvData)
		//fmt.Println("Bytes: ", n, "Data: ", recvData, string(recvData))
		if n <= 0 || err != nil || strings.Contains(out, expect) {
			break
		} else {
			out += string(recvData)
		}
	}
	return out
}

// Функция принимает Telnet сессию и команду, которую следует отправить
// текст команды преобразуется в byte и отправляется на устройство,
// после чего также отправляется \r\n
func SenderTelnet(conn *telnet.Conn, command string) {
	var commandBuffer []byte
	for _, char := range command {
		commandBuffer = append(commandBuffer, byte(char))
	}

	var crlfBuffer [2]byte = [2]byte{'\r', '\n'}
	crlf := crlfBuffer[:]

	//fmt.Println(commandBuffer)

	conn.Write(commandBuffer)
	conn.Write(crlf)
}

// https://github.com/reiver/go-telnet/blob/master/standard_caller.go
// https://godoc.org/github.com/reiver/go-telnet
//запрашиваем IP адрес, логин, пароль и команду, которую следует ввести
//запускаем пинг до устройсва
//после того, как получен ответ, открываем Telnet сессию и отправляем команды
func Runner() {

	ipAdd := Requester("IP address")
	login := Requester("login")
	passwd := Requester("password")
	cmd := Requester("command to execute")

	if Pinger(ipAdd, 60) {
		ipAdd += ":23"
		conn, err := telnet.DialTo(ipAdd)
		if nil != err {
			fmt.Println(err)
		}
		defer conn.Close()

		fmt.Print(ReaderTelnet(conn, "Login"))
		SenderTelnet(conn, login)
		fmt.Print(ReaderTelnet(conn, "Password"))
		SenderTelnet(conn, passwd)
		fmt.Print(ReaderTelnet(conn, ">"))
		SenderTelnet(conn, cmd)
		fmt.Print(ReaderTelnet(conn, ">"))

		return
	}

}

func main() {
	fmt.Println("Welcome to Mortal Coil.")
	fmt.Println("Hit Enter TWICE after entering the command to execute")
	fmt.Println("In Windows 8 run this program with administrative priviledges\n")

	for {
		Runner()
		fmt.Println("\nPress  \"e\" to exit or any other button to repeat")
		if Reader() == "e" {
			break
		}
	}

}
